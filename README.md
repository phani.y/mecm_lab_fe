# mecm-fe
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
![Jenkins](https://img.shields.io/jenkins/build?jobUrl=http%3A%2F%2Fjenkins.edgegallery.org%2Fview%2FMEC-PLATFORM-BUILD%2Fjob%2Fmecm-meo-frontend-docker-image-update-daily-master%2F)

#### 介绍
repo for mecm fe

#### 软件架构
See [Configuration Reference](https://cli.vuejs.org/config/).


#### 安装教程

1. npm install


#### 使用说明

- Project setup  
npm install
- Compiles and hot-reloads for development  
npm run serve
- Compiles and minifies for production  
npm run build
- Run your tests  
npm run test
- Lints and fixes files  
npm run lint

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 链接

1.  MECM访问地址：[http://mecm.edgegallery.org](http://mecm.edgegallery.org)
2.  EdgeGallery官方网站：[http://www.edgegallery.org/](http://www.edgegallery.org/)
3.  EdgeGallery社区地址：[http://mecm.edgegallery.org](http://mecm.edgegallery.org)
4.  邮箱: contact@edgegallery.org
