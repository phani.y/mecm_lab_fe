/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
const Mock = require('mockjs')
const userData = function () {
  return [
    {
      'userId': '######',
      'userName': '******',
      'isSecureBackend': 'true',
      'loginPage': '',
      'accessToken': '######',
      'authorities': []
    }
  ]
}

const loginData = function () {
  return [
    {
      'userId': '10000',
      'username': 'administrator',
      'password': 'administrator',
      'role': 'administrator'
    },
    {
      'userId': '10001',
      'username': 'appdeveloper',
      'password': 'appdeveloper',
      'role': 'app developer'
    },
    {
      'userId': '10002',
      'username': 'communitydeveloper',
      'password': 'communitydeveloper',
      'role': 'community developer'
    }
  ]
}

const hostData = function () {
  return [
    {
      address: '78hao',
      affinity: 'X86',
      applcmIp: '119.8.53.3',
      city: '北京市/北京市/东城区/东华门街道',
      edgerepoIp: '119.8.53.3',
      edgerepoName: null,
      edgerepoPort: '8123',
      edgerepoUsername: '',
      mechostIp: '119.8.53.3',
      mechostName: 'Node',
      userName: '',
      zipCode: ''
    }
  ]
}

const packageData = function () {
  return [ {
    affinity: 'GPU',
    appId: '73b6f66992ab4798a55350f419a210af',
    contact: null,
    createTime: '2020-09-14 20:00:57.29812',
    details: '',
    downloadCount: 25,
    iconUrl: null,
    industry: 'Smart Park',
    name: 'zone-swr-test1',
    provider: 'Huawei',
    score: 5,
    shortDesc: 'for testing',
    type: 'Video Application',
    userId: '92e5d627-a501-479b-922a-8e63eb92cf57',
    userName: 'wenson'
  }]
}

const instanceInfo = function () {
  return {
    'response': [
      {
        'appInstanceId': '4b6944f1-655c-4f77-9e86-c0c4eb629c9c',
        'appPackageId': '9eb381e222dd4a3898fc61f7182bba24',
        'appName': 'zone',
        'appId': '73b6f66992ab4798a55350f419a210af',
        'appDescriptor': 'zone',
        'mecHost': '119.8.53.3',
        'applcmHost': '119.8.53.3',
        'operationalStatus': 'Instantiated',
        'operationInfo': 'success'
      }
    ]
  }
}

const kpiInfo = function () {
  let data = { 'cpuusage': { 'total': 1600653686.904, 'used': '0.025' }, 'memusage': { 'total': 1600653686.906, 'used': '0.004406774826102075' }, 'diskusage': { 'total': '0.0', 'used': '0.0' } }
  return {
    response: JSON.stringify(data)
  }
}

const packageInfo = function () {
  return {
    'csarId': '9eb381e222dd4a3898fc61f7182bba24',
    'downloadUrl': '/home/appstore/7b002245635b4563a3d4dd4c96168556/7b002245635b4563a3d4dd4c96168556.csar',
    'iconUrl': '/home/appstore/70d3278cbf654377beeb3d178ad01ff3/70d3278cbf654377beeb3d178ad01ff3.png',
    'size': '0',
    'createTime': '2020-09-14 20:00:57.29812',
    'name': 'zone-swr-test1',
    'version': '1.0',
    'type': 'Video Application',
    'details': '',
    'affinity': 'GPU',
    'industry': 'Smart Park',
    'contact': null,
    'appId': '73b6f66992ab4798a55350f419a210af'
  }
}

const distributionData = function () {
  return [{
    'appPkgId': '9eb381e222dd4a3898fc61f7182bba24',
    'appPkgName': 'zone-swr-test1',
    'appPkgVersion': '1.0',
    'appPkgPath': 'https://appstore-be-svc:8099/mec/appstore/v1/apps/73b6f66992ab4798a55350f419a210af/packages/9eb381e222dd4a3898fc61f7182bba24/action/download',
    'appProvider': 'Huawei',
    'appPkgDesc': 'for testing',
    'appPkgAffinity': 'GPU',
    'appIconUrl': 'https://appstore-be-svc:8099/mec/appstore/v1/apps/73b6f66992ab4798a55350f419a210af/icon',
    'createdTime': '2020-09-19T07:31:33.267',
    'modifiedTime': '2020-09-19T07:31:33.455',
    'appId': '73b6f66992ab4798a55350f419a210af',
    'mecHostInfo': [{ 'hostIp': '119.8.53.3', 'status': 'Distributed', 'error': '' }]
  }]
}

const applcmData = function () {
  return [
    {
      'ip': '119.8.125.174',
      'port': '30101',
      'managedMecHost': null,
      'username': 'wenson',
      'password': '######',
      'userId': '7269638e-5637-4b8c-8178-b5112ba7b69b'
    },
    { 'ip': '159.138.140.246',
      'port': '30101',
      'managedMecHost': null,
      'username': '',
      'password': '',
      'userId': '7269638e-5637-4b8c-8178-b5112ba7b69b'
    }
  ]
}

const appstoreData = function () {
  return [
    { 'url': 'https://appstore.edgegallery.com',
      'username': 'wenson',
      'password': '######',
      'appstorename': 'huaweiappstore',
      'producer': 'huawei',
      'time': '01-09-2020 09:04:04',
      'userId': '7269638e-5637-4b8c-8178-b5112ba7b69b'
    }
  ]
}

const serviceInfoData = function () {
  let data =
  {
    'pods':
    [
      {
        'podstatus': 'Running',
        'podname': 'zoon-minder-6c99b874-wc8zn',
        'containers': [
          { 'containername': 'zoonminder',
            'metricsusage': {
              'cpuusage': '83538/4000000',
              'memusage': '167305216/16656244736',
              'diskusage': '0/100276072165'
            }
          }
        ]
      }, {
        'podstatus': 'Running',
        'podname': 'zoon-minder-db-67dbdfcfc-2vlvz',
        'containers': [
          {
            'containername': 'zoondb',
            'metricsusage': {
              'cpuusage': '340110/4000000',
              'memusage': '270934016/16656244736',
              'diskusage': '0/100276072165'
            }
          }
        ]
      }
    ]
  }

  let res = { response: JSON.stringify(data) }
  return res
}

const alarmData = function () {
  let alarm = {
    '100000': {
      'Overall': 13100,
      'Critical': 6700,
      'Major': 3600,
      'Minor': 2800
    },
    '110000': {
      'Overall': 1000,
      'Critical': 650,
      'Major': 300,
      'Minor': 50
    },
    '650000': {
      'Overall': 12100,
      'Critical': 6050,
      'Major': 3300,
      'Minor': 2750
    },
    '652800': {
      'Overall': 11000,
      'Critical': 5500,
      'Major': 3000,
      'Minor': 2500
    },
    '653200': {
      'Overall': 1100,
      'Critical': 550,
      'Major': 300,
      'Minor': 250
    }
  }
  return alarm
}
const userDataList = function () {
  return [ {
    username: 'Wenson',
    userId: '73b6f66992ab4798a55350f419a210af',
    telephone: '029-3333333**',
    company: 'Huawei',
    email: 'XXXx@ccc.com',
    location: 'ShenZen',
    role: 'Administrator'
  },
  {
    username: 'David',
    userId: '73b6f66992ab4798a55350f419a210ag',
    telephone: '029-5555555**',
    company: 'xxx',
    email: 'XXXx@ccc.com',
    location: 'BeiJing',
    role: 'App Developer'
  },
  {
    username: 'Joe',
    userId: '73b6f66992ab4798a55350f419a210ak',
    telephone: '029-1111111**',
    company: 'xx',
    email: 'XXXx@ccc.com',
    location: 'ShangHai',
    role: 'Community Developer'
  }]
}
const envDataList = function () {
  return [ {
    name: 'mec-wenson-dev',
    lab: 'Huawei Shenzen lab',
    status: 'waiting for operation',
    specifications: '4vCPUs|16GB|s3xlarge.4|Ubuntu 18.04 server 64bit',
    ipAddress: '1.1.1.1',
    testOnSite: 'yes',
    testCard: '5',
    specialResources: 'GPU',
    applicant: 'ywx837623',
    requestedduration: '1 week'
  },
  {
    name: 'mec-wenson-dev',
    lab: 'Huawei Shenzen lab',
    status: 'waiting for operation',
    specifications: '4vCPUs|16GB|s3xlarge.4|Ubuntu 18.04 server 64bit',
    ipAddress: '1.1.1.1',
    testOnSite: 'yes',
    testCard: '5',
    specialResources: 'GPU',
    applicant: 'ywx837623',
    requestedduration: '1 week'
  },
  {
    name: 'mec-wenson-dev',
    lab: 'Huawei Shenzen lab',
    status: 'waiting for operation',
    specifications: '4vCPUs|16GB|s3xlarge.4|Ubuntu 18.04 server 64bit',
    ipAddress: '1.1.1.1',
    testOnSite: 'yes',
    testCard: '5',
    specialResources: 'GPU',
    applicant: 'ywx837623',
    requestedduration: '1 week'
  }]
}
const resDataList = function () {
  return [ {
    name: 'mec-wenson-dev',
    lab: 'Huawei Shenzen lab',
    status: 'waiting for operation',
    specifications: '4vCPUs|16GB|s3xlarge.4|Ubuntu 18.04 server 64bit',
    specialResources: 'GPU',
    testOnSite: 'yes',
    testCard: '5',
    requestedduration: '1 week',
    applicant: 'ywx837623',
    remark: '2 camera, 1vr glass'
  },
  {
    name: 'EG-dev-test',
    lab: 'ZiJinshan',
    status: 'Approved',
    specifications: '4vCPUs|16GB|s3xlarge.4|Ubuntu 18.04 server 64bit',
    specialResources: 'NA',
    testOnSite: 'No',
    testCard: '0',
    requestedduration: '20 days',
    applicant: 'xxx',
    remark: '2 camera, 1vr glass'
  },
  {
    name: 'EC-zwx87733-development',
    lab: 'ZiJinshan',
    status: 'Rejected',
    specifications: '4vCPUs|16GB|s3xlarge.4|Ubuntu 18.04 server 64bit',
    specialResources: 'GPU',
    testOnSite: 'No',
    testCard: '0',
    requestedduration: '1 month',
    applicant: 'xxx',
    remark: '2 camera, 1vr glass'
  }]
}

const appDevReqList = function () {
  return [ {
    name: 'mec-wenson-dev',
    lab: 'Huawei Shenzen lab',
    status: 'waiting for operation',
    scenario: 'GPU',
    specifications: '4vCPUs|16GB|s3xlarge.4|Ubuntu 18.04 server 64bit',
    ipAddress: '1.1.1.1',
    requestedduration: '1 week',
    specialResources: 'GPU'
  }]
}

const appDevEnvList = function () {
  return [ {
    name: 'mec-wenson-dev',
    lab: 'Huawei Shenzen lab',
    status: 'waiting for operation',
    specifications: '4vCPUs|16GB|s3xlarge.4|Ubuntu 18.04 server 64bit',
    ipAddress: '1.1.1.1',
    specialResources: 'GPU'
  }]
}

const comDevReqList = function () {
  return [ {
    name: 'mec-wenson-dev',
    lab: 'Huawei Shenzen lab',
    status: 'waiting for operation',
    scenario: 'GPU',
    specialResources: 'GPU',
    ipAddress: '1.1.1.1',
    testOnSite: 'yes',
    testCard: '5',
    requestedduration: '1 week',
    remark: '2 camera, 1vr glass'
  }]
}

const comDevResList = function () {
  return [ {
    name: 'mec-wenson-dev',
    lab: 'Huawei Shenzen lab',
    status: 'waiting for operation',
    specifications: '4vCPUs|16GB|s3xlarge.4|Ubuntu 18.04 server 64bit',
    expireddate: '2020/12/31 23:59:59',
    specialResources: 'GPU'
  }]
}

Mock.mock('/mock/login', userData)
Mock.mock('/mock/mechosts', hostData)
Mock.mock('/mock/appPackageList', packageData)
Mock.mock('/mock/appDistributionList', distributionData)
Mock.mock('/mock/applcms', applcmData)
Mock.mock('/mock/appstores', appstoreData)
Mock.mock('/mock/alarmData', alarmData)
Mock.mock('/mock/seviceInfo', serviceInfoData)
Mock.mock('/mock/instanceInfo', instanceInfo)
Mock.mock('/mock/packageInfo', packageInfo)
Mock.mock('/mock/kpiInfo', kpiInfo)
Mock.mock('/mock/userInfo', userDataList)
Mock.mock('/mock/resInfo', resDataList)
Mock.mock('/mock/envInfo', envDataList)
Mock.mock('/mock/reqInfo', resDataList)
Mock.mock('/mock/comDevResList', comDevResList)
Mock.mock('/mock/comDevReqList', comDevReqList)
Mock.mock('/mock/appDevEnvList', appDevEnvList)
Mock.mock('/mock/appDevReqList', appDevReqList)
Mock.mock('/login', loginData)
