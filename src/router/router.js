/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '',
      name: 'home',
      component: Home,
      children: [
        {
          path: '/',
          name: 'login',
          component: () => import('../login/Login.vue')
        },
        {
          path: '/login',
          name: 'login',
          component: () => import('../login/Login.vue')
        },
        {
          path: '/registration',
          name: 'registration',
          component: () => import('../login/Register.vue')
        },
        {
          path: '/help',
          name: 'help',
          component: () => import('../administrator/Help.vue')
        },
        {
          path: '/administrator/home',
          name: 'adminhome',
          component: () => import('../administrator/Home.vue')
        },
        {
          path: '/administrator/resourcelist',
          name: 'resourcelist',
          component: () => import('../administrator/ResourceList.vue')
        },
        {
          path: '/administrator/requestlist',
          name: 'requestlist',
          component: () => import('../administrator/RequestList.vue')
        },
        {
          path: '/administrator/usermanagement',
          name: 'usermanagement',
          component: () => import('../administrator/UserManagement.vue')
        },
        {
          path: '/app/home',
          name: 'apphome',
          component: () => import('../developer/app/Home.vue')
        },
        {
          path: '/app/myenvironment',
          name: 'myenvironment',
          component: () => import('../developer/app/MyEnvironment.vue')
        },
        {
          path: '/app/myrequest',
          name: 'apprequest',
          component: () => import('../developer/app/MyRequest.vue')
        },
        {
          path: '/community/home',
          name: 'communityhome',
          component: () => import('../developer/community/Home.vue')
        },
        {
          path: '/community/myresource',
          name: 'communityresource',
          component: () => import('../developer/community/MyResource.vue')
        },
        {
          path: '/community/myrequest',
          name: 'communityrequest',
          component: () => import('../developer/community/MyRequest.vue')
        },
        {
          path: '/community/certify',
          name: 'communitycertify',
          component: () => import('../developer/community/Certify.vue')
        },
        {
          path: '/guest/home',
          name: 'guesthome',
          component: () => import('../guest/Home.vue')
        }
      ]
    }
  ]
})
