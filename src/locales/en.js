/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import enLocale from 'element-ui/lib/locale/lang/en'
const en = {
  login: {
    userName: 'Username',
    password: 'Password',
    login: 'Log In',
    logout: 'Log Out',
    wrongCaptcha: 'Invalid captcha, please recheck the Captcha.',
    confirmPass: 'Confirm Password',
    gender: 'Gender',
    male: 'Male',
    female: 'Female',
    pwdPla: 'Password',
    pwdConfPla: 'Confirm password',
    compPla: 'Company',
    telPla: 'Telephone number',
    loginWithUser: 'Edge Gallery Lab User Login',
    freeSign: 'Register User',
    verify: 'Drag to verify',
    finishVerify: 'Verified'
  },
  user: {
    username: 'User Name',
    role: 'Role',
    company: 'Company',
    location: 'Company Address',
    telephone: 'Telephone',
    email: 'Email',
    operation: 'Operation',
    batchDltBtn: 'Batch Delete',
    addUsrBtn: 'Add User',
    edit: 'Edit',
    delete: 'Delete',
    guestconfirguration: 'Guest Configure'
  },
  nav: {
    administrator: 'Administrator',
    resourcelist: 'Resource Manager',
    requestlist: 'Request Manager',
    usermanagement: 'User Manager',
    lsms: 'Lab Service Management System',
    myapplication: 'My Applications',
    resapplication: 'Resource Application',
    resapplicationbtn: 'Request Resource',
    batchprocessing: 'Batch Processing',
    batchRejects: 'Batch Rejects',
    batchApprove: 'Batch Approve',
    envapplication: 'Environment Application',
    envapplicationbtn: 'Request Environment',
    system: 'System',
    logoutTip: 'Are you sure to log out?',
    rejects: 'Rejects',
    approveapplication: 'Approve the application',
    myres: 'My Resources',
    listofauthapplications: 'List of Authenticated Applications',
    mecmanager: 'MEC Manager Link',
    mecapp: 'MEC APP Link',
    mecdeveloper: 'MEC Developer Link',
    addguestlink: 'Guest Configuration',
    beaguest: 'Be a Guest'
  },
  common: {
    reset: 'Reset',
    search: 'Search',
    operation: 'Operation',
    cancel: 'Cancel',
    submit: 'Submit',
    confirm: 'Confirm',
    delete: 'Delete',
    detail: 'Detail',
    modify: 'Modify',
    warning: 'Promt',
    add: 'Add',
    labDistribution: 'Labs Distribution',
    monitorLab: 'Lab Details',
    total: 'Total:',
    labs: 'Labs',
    lab: 'Lab',
    environments: 'Environments',
    resources: 'Resources'
  },
  verify: {
    ipTip: 'Ip should not be empty',
    hostnameTip: 'Host name should not be empty',
    zipcodeTip: 'Zipcode should not be empty',
    cityTip: 'City should not be empty',
    addressTip: 'Address Mec Host should not be empty',
    usernameTip: 'User name should not be empty',
    passwordTip: 'Password should not be empty',
    affinityTip: 'Architecture should not be empty'
  },
  env: {
    requestRemarks: 'Requested Remarks',
    approvalRemarks: 'Approval Remarks',
    requestedBy: 'Requested By',
    nodeType: 'Node Type',
    storage: 'Storage',
    assignedTo: 'Assigned To',
    owner: 'Administrator',
    pinCode: 'Zipcode',
    city: 'City',
    address: 'Address',
    type: 'Type',
    name: 'Name',
    nameid: 'Name/ID',
    lab: 'Lab',
    status: 'Status',
    specifications: 'Specifications',
    specialResources: 'Special Resource',
    testOnSite: 'Test Onsite',
    testCard: 'Test Card',
    requestedduration: 'Request Duration',
    applicant: 'Applicant',
    remark: 'Remarks',
    addres: 'Add Resource',
    addenv: 'Add Environment',
    addlab: 'Add Lab',
    ipAddress: 'IP Address',
    expiredDate: 'Expired Date',
    scenario: 'Scenario',
    operation: 'Operation',
    cancelApplication: 'Cancel Application',
    details: 'Details',
    application: 'Application',
    apply: 'Apply VPN',
    myresource: 'Resource Application',
    myenvironment: 'Environment Application',
    myrequests: 'My Applications',
    applicationRes: 'Resource Application',
    remotelogin: 'Remote login',
    release: 'Release',
    resuming: 'Resuming',
    rejectionreason: 'Reason of rejection',
    remarkDetails: 'Enter the remark. The value cannot exceed 100 characters...',
    applicationName: 'Application Name',
    architacture: 'Architecture',
    developer: 'Developer',
    description: 'Description',
    isAllowed: 'IsAllowed',
    duration: 'Valid Till',
    testonsitelabel: 'Whether to perform the test on site',
    specificationSection: 'Specification Section',
    no: 'No',
    quantity: 'Quantity',
    cpuArchitacture: 'CPU architacture',
    memory: 'Memory',
    labMonitor: 'Lab Monitor',
    totalEnvironments: 'Total Environments:',
    totalResources: 'Total Resources:'

  },
  tip: {
    getCommonListFailed: 'Get list failed',
    failedReg: 'Failed to register user, ',
    failedAuth: 'Authentication Failure, please recheck the User Name or Password.',
    regUserSuc: 'User is registered successfully!',
    sessionOut: 'Your session is expired.',
    sessionExp: 'Session Expired',
    warning: 'Warning',
    browserAdvise: 'For sure some functions work well, please use Chrome',
    confirm: 'Confirm',
    nameAlSinged: 'The username number has been already registered, please change another one',
    telAlSigned: 'The telephone number has been already registered, please change another one',
    invalidIP: 'Invalid IP address.'
  },
  confirm: {
    registerUserSuccess: 'User registered successfully!!',
    userLoginSuccess: 'User logged-in successfully!!',
    confirmToDeleteUser: 'Are you sure to delete user?',
    confirmToDeleteEnv: 'Are you sure to delete this environment?',
    confirmToDeleteEnvAssinged: 'Environment is assigned to app developer, do you want to delete?',
    confirmToDeleteRes: 'Are you sure to delete this resource?',
    confirmToDeleteLab: 'Are you sure to delete this lab?',
    confirmToReleaseRes: 'Are you sure to release the resource?',
    confirmToReleaseEnv: 'Are you sure to release the environment?',
    confirmCancelReq: 'Are you sure to cancel the request?',
    deleteEnvSuccess: 'Environment deleted successfully.',
    deleteResSuccess: 'Resource deleted successfully.',
    labAddSuccess: 'New lab added successfully.',
    addUserSuccess: 'New user added successfully.',
    deleteUserSuccess: 'User deleted successfully.',
    deleteBatchUserSuccess: 'Selected Users deleted successfully.',
    envRequestSuccess: 'Environment requested successfully.',
    resRequestSuccess: 'Resourse requested successfully.',
    envReleaseSuccess: 'Environment released successfully.',
    guestConfigSuccess: 'Successfully configured the guest page.',
    approveConfim: 'Request approved successfully.',
    deleteLabConfim: 'Lab deleted successfully.',
    deleteResRequestSuccess: 'Resource requested deleted successfully.',
    resReleaseSuccess: 'Resource released successfully',
    confirmApplyEnv: 'Please wait for 24 hours, admin will review your changes and process the request.',
    selectScenario: 'Please select scenario.',
    selectEnv: 'Please select environment from table.'

  },
  Available: 'Available',
  Assigned: 'Assigned',
  Waiting: 'Waiting',
  Approved: 'Approved',
  Rejected: 'Rejected',
  RESOURCE_EXISTS: 'Resource ip is already exist. Please use another one.',
  MODIFY_RESOURCE_FAILED: 'Failed to modify the resource.',
  DELETE_RESOURCE_FAILED: 'Failed to delete the resource.',
  DELETE_RESOURCE_REQUEST_FAILED: 'Failed to delete the resource request.',
  RELEASE_RESOURCE_FAILED: 'Failed to release the resource.',
  ENV_NAME_ALREADY_EXIST: 'Environment name is already exist. Please use another one.',
  ADD_ENVIRONMENT_FAILED: 'Failed to add new environment.',
  MODIFY_ENVIRONMENT_FAILED: 'Failed to modify the environment.',
  DELETE_ENVIRONMENT_FAILED: 'Failed to delete the environment.',
  RELEASE_ENVIRONMENT_FAILED: 'Failed to release the environment.',
  ADD_LAB_FAILED: 'Failed to add new lab.',
  DELETE_LAB_FAILED: 'Failed to delete lab.',
  ADD_USER_FAILED: 'Failed to add new user.',
  DELETE_USER_FAILED: 'Failed to delete user.',
  DELETE_BATCH_USER_FAILED: 'Some of the selected users not able to delete.',
  MODIFY_USER_FAILED: 'Failed to modify the user.',
  REGISTRATION_INFO_INCORRECT: 'Registration information is not correct.',
  REGISTER_USER_FAILED: 'Failed to register.',
  USER_ALREADY_EXIST: 'User already exist, Please user another username.',
  USER_NOT_ALLOWED: 'You are not allow to login. Please contact to administrator.',
  USERNAME_PASSWORD_DOES_NOT_EXIST: 'User name and password is incorrect.',
  MODIFY_GUEST_CONFIG_FAILED: 'Failed to modify the guest configuration.',
  ENVIRONMENT_REQUEST_ALREADY_EXIST: 'You have already requested for this environment. Please select another one.',
  LAB_ALREADY_EXIST: 'Lab already exist. Please try another one.',
  ADD_RESOURCE_LIST: 'No resource added. Please add one resource',
  RESOURCE_ALREADY_ASSIGNED: 'Resource cannot delete, assigned to environment.',
  ...enLocale
}

export default en
