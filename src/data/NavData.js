/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const NavData = [
  {
    'id': '2.2',
    'name': 'Login',
    'path': '/login',
    'pageId': '2.0.1',
    'display': false,
    'children': [
      {
        'id': '2.3',
        'name': 'Login',
        'path': '/login',
        'pageId': '2.0.1.1',
        'display': false
      },
      {
        'id': '2.5',
        'name': 'Registration',
        'path': '/registration',
        'pageId': '2.0.1.3',
        'display': false
      }
    ]
  },
  {
    'id': '2.2',
    'name': 'Administrator',
    'path': '/administrator/home',
    'pageId': '2.0.1',
    'display': false,
    'children': [
      {
        'id': '2.3',
        'name': 'Home',
        'path': '/administrator/home',
        'pageId': '2.0.1.1',
        'display': false
      },
      {
        'id': '2.5',
        'name': 'Resource List',
        'path': '/administrator/resourcelist',
        'pageId': '2.0.1.3',
        'display': false
      },
      {
        'id': '2.5',
        'name': 'User Management',
        'path': '/administrator/usermanagement',
        'pageId': '2.0.1.3',
        'display': false
      }
    ]
  },
  {
    'id': '2.4',
    'name': 'Developer',
    'path': '/',
    'display': false,
    'children': [
      {
        'id': '2.4.1',
        'name': 'App Developer',
        'path': '/app/home',
        'pageId': '2.0.3.1',
        'display': false,
        children: [
          {
            'id': '2.4.1.1',
            'name': 'Home',
            'path': '/app/home',
            'pageId': '2.0.3.1.1',
            'display': false
          },
          {
            'id': '2.4.1.2',
            'name': 'My Resource',
            'path': '/app/myresource',
            'pageId': '2.0.3.1.2',
            'display': false
          },
          {
            'id': '2.4.1.3',
            'name': 'My Request',
            'path': '/app/myrequest',
            'pageId': '2.0.3.1.3',
            'display': false
          },
          {
            'id': '2.4.1.3',
            'name': 'Env Request',
            'path': '/app/envrequest',
            'pageId': '2.0.3.1.3',
            'display': false
          }
        ]
      },
      {
        'id': '2.4.1',
        'name': 'Community Developer',
        'path': '/community/home',
        'pageId': '2.0.3.1',
        'display': false,
        children: [
          {
            'id': '2.4.1.1',
            'name': 'Home',
            'path': '/community/home',
            'pageId': '2.0.3.1.1',
            'display': false
          },
          {
            'id': '2.4.1.2',
            'name': 'My Resource',
            'path': '/community/myresource',
            'pageId': '2.0.3.1.2',
            'display': false
          },
          {
            'id': '2.4.1.3',
            'name': 'My Request',
            'path': '/community/myrequest',
            'pageId': '2.0.3.1.3',
            'display': false
          }
        ]
      }
    ]
  }
]

export default NavData
