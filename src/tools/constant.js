/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
const BASE_URL = `http://${window.location.href.split('//')[1].split(':')[0]}:9006`

const CPU_ARCH = [
  {
    label: 'x86',
    value: 'x86'
  },
  {
    label: 'ARM',
    value: 'ARM'
  }
]

const SPECIAL_RES = [
  {
    label: 'GPU',
    value: 'GPU'
  },
  {
    label: 'NPU',
    value: 'NPU'
  }
]

const ROLES = [
  {
    label: 'ALL',
    value: 'all'
  },
  {
    label: 'Administrator',
    value: 'administrator'
  },
  {
    label: 'App Developer',
    value: 'appdeveloper'
  }
]

const RES_STATUS = [
  'Availble',
  'Assigned'
]

const REQ_STATUS = [
  'Waiting',
  'Aproved',
  'Rejected'
]

const NODE_TYPE = [
  'Edge Node',
  'Central Node'
]

const TEST_ON_SITE = {
  0: 'NO',
  1: 'YES'
}

const DURATION_LIST = [
  '1 WEEK',
  '1 MONTH',
  '1 YEAR'
]

const STORAGE_LIST = ['500GB', '1TB', '10TB']

const CPU_LIST = ['2 vCPUs', '4 vCPUs', '8 vCPUs', '16 vCPUs', '32 vCPUs']

const MEMORY_LIST = ['8 GB', '16 GB', '64 GB', '128 GB']

const SYSTEM_LIST = ['Ubuntu 16.04', 'Ubuntu 18.04', 'Ubuntu 20.04']

export {
  ROLES,
  RES_STATUS,
  REQ_STATUS,
  NODE_TYPE,
  TEST_ON_SITE,
  DURATION_LIST,
  CPU_LIST,
  MEMORY_LIST,
  SYSTEM_LIST,
  STORAGE_LIST,
  BASE_URL,
  CPU_ARCH,
  SPECIAL_RES
}
